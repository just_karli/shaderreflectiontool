﻿using System;
using ShaderReflectionLib;
using SharpDX.D3DCompiler;

namespace ShaderReflectionConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                PrintInputParameter();
                return;
            }
            
            string file = args[0];
            string entry_point = args[1];
            string shader_profile = args[2];

            var loader = new ShaderLoader();
            var summary = loader.LoadShader(file, entry_point, shader_profile, ShaderFlags.Debug | ShaderFlags.OptimizationLevel0);

            Console.WriteLine(summary.InputDescription);

            foreach (var cbuffers in summary.ConstBuffers)
                Console.WriteLine(cbuffers);
            
            Console.WriteLine(summary.ResourceBindings);

            Console.ReadLine();
        }
        
        private static void PrintInputParameter()
        {
             Console.WriteLine("usage: <filepath> <shader_entrypoint> <shaderprofile>");
        }
    }
}
