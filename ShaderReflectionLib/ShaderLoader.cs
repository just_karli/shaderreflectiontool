﻿using System.IO;
using SharpDX.D3DCompiler;

namespace ShaderReflectionLib
{
    public class ShaderLoader
    {
        public ShaderSummary LoadShader(string file, string entry_point, string shader_profile, ShaderFlags flags)
        {
            if (File.Exists(file))
            {
                var bytecode = ShaderBytecode.CompileFromFile(file, entry_point, shader_profile, flags);
                var reflector = new ShaderReflection(bytecode);

                var summary = new ShaderSummary();
                summary.ShaderDescription = reflector.Description;
                summary.InputDescription = GetShaderInputParameter(reflector, reflector.Description.InputParameters);
                summary.ConstBuffers = GetConstantBuffer(reflector, reflector.Description.ConstantBuffers);
                summary.ResourceBindings = GetResourceBindings(reflector, reflector.Description.BoundResources);
                
                return summary;
            }

            throw new FileNotFoundException(file);
        }

        public ShaderInput GetShaderInputParameter(ShaderReflection reflector, int parameter_count)
        {
            var input = new ShaderInput(parameter_count);
            for (int i = 0; i < parameter_count; i++)
                input.Parameter[i] = reflector.GetInputParameterDescription(i);
            
            return input;
        }

        public ShaderConstantBuffer[] GetConstantBuffer(ShaderReflection reflector, int constant_buffer_count)
        {
            var cbuffer_summary = new ShaderConstantBuffer[constant_buffer_count];
            for (int i = 0; i < constant_buffer_count; i++)
            {
                var cbuffer = new ShaderConstantBuffer();
                cbuffer.ConstantBuffer = reflector.GetConstantBuffer(i);
                cbuffer.Variables = new ShaderReflectionVariable[cbuffer.ConstantBuffer.Description.VariableCount];

                for (int j = 0; j < cbuffer.Variables.Length; j++)
                    cbuffer.Variables[j] = cbuffer.ConstantBuffer.GetVariable(j);

                cbuffer_summary[i] = cbuffer;
            }

            return cbuffer_summary;
        }

        public ShaderResourceBindings GetResourceBindings(ShaderReflection reflector, int resource_binding_count)
        {
            var input_bindings = new InputBindingDescription[resource_binding_count];
            for (int i = 0; i < resource_binding_count; i++)
                input_bindings[i] = reflector.GetResourceBindingDescription(i);

            return new ShaderResourceBindings { ResourceBindings = input_bindings };
        }
    }

}
