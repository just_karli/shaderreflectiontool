﻿using SharpDX.D3DCompiler;

namespace ShaderReflectionLib
{
    public class ShaderResourceBindings
    {
        public override string ToString()
        {
            if (ResourceBindings == null || ResourceBindings.Length == 0)
                return "No resource bindings with this shader";

            string description = "Resource Bindings: ";

            foreach (var bindings in ResourceBindings)
                description += "\n\t" + bindings.Name + " : " + bindings.Type + " bindpoint: " + bindings.BindPoint;

            return description;
        }

        public InputBindingDescription[] ResourceBindings { get; set; }
    }
}
