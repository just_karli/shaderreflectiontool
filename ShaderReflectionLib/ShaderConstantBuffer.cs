﻿using SharpDX.D3DCompiler;

namespace ShaderReflectionLib
{
    public class ShaderConstantBuffer
    {
        public override string ToString()
        {
            if (ConstantBuffer == null && Variables == null)
                return "Unset cbuffer";

            string description = "ConstantBuffer: \n\t" + ConstantBuffer.Description.Name +
                                 " ( " + ConstantBuffer.Description.Size + " )";

            foreach (var shader_variable in Variables)
            {
                description += "\n\t\t" + shader_variable.Description.Name + " : " +
                               shader_variable.GetVariableType().Description.Name + " ( " +
                               shader_variable.GetVariableType().Description.Type + " )";
            }

            return description;
        }

        public ConstantBuffer ConstantBuffer { get; set; }
        public ShaderReflectionVariable[] Variables { get; set; }
    }
}
