﻿using SharpDX.D3DCompiler;

namespace ShaderReflectionLib
{
    public class ShaderInput
    {
        public ShaderInput(int parameter_count)
        {
            Parameter = new ShaderParameterDescription[parameter_count];    
        }

        public override string ToString()
        {
            if (Parameter == null || Parameter.Length == 0)
                return "No shader input available";

            string description = "Input: ";

            foreach (var shader_parameter in Parameter)
            {
                description += "\n\t" + shader_parameter.SemanticName + " t: " + shader_parameter.ComponentType +
                               " u: " + shader_parameter.UsageMask;
            }

            return description;
        }

        public ShaderParameterDescription[] Parameter { get; set; } 
    }
}
