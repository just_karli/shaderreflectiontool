﻿using SharpDX.D3DCompiler;

namespace ShaderReflectionLib
{
    public class ShaderSummary
    {
        public ShaderDescription ShaderDescription { get; set; }
        public ShaderInput InputDescription { get; set; }

        public ShaderConstantBuffer[] ConstBuffers { get; set; }
        public ShaderResourceBindings ResourceBindings{ get; set; }
    }
}
